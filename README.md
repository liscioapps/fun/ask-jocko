# Ask Jocko
This project is to help Jocko answer the questions he's already answered a million times.  And help people GET AFTER IT by finding the answers to their questions as quickly as possible.

## Technology Stack
* [Node.js](https://nodejs.org/en/)
* [MongoDB](https://www.mongodb.com/)
* [Jekyll](http://jekyllrb.com/)
* [MaterilizeCSS](http://materializecss.com/)
* [SASS](http://sass-lang.com/)
* [Docker]()
* [GitLab Registry](https://gitlab.com/help/user/project/container_registry)
* [Kubernetes]()

## Structure
The project is broken into the following services by folder

```
   /twit-service      This is the service that accesses Twitter and loads Jocko's tweets into MongoDB
   /UI-comming-soon   This is a Jekyll static site as a placeholder for ask-jocko.com
   /font-end          This is where the front-end search project is stored
   /ask-jocko-api     This is the API for searching & finding tweets
```