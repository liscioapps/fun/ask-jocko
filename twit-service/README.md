# Twitter Access Service
This service will access twitter an pump the tweets into the MongoDB

## Setup
### Prerequisites 
To set up your dev environment you'll need:
* [Node.js](https://nodejs.org/en/download/)
* [MongoDB](https://docs.mongodb.com/manual/installation/)
* [Twitter API keys](https://apps.twitter.com/)

> Recommendation: [set up Mongo as a service on OS X](https://stackoverflow.com/a/5601077).

### .env.js
There are a number of environment variables required.  In production you should store these as secrets (for instance on [Heroku](https://devcenter.heroku.com/articles/config-vars)). However, this is annoying in DEV so we've created the concept of a `.env.js` file that will be called to set up your environment for you.

However, this file is NOT stored in GIT (see .gitignore) because it contains sensitive information (like your Twitter API keys).  So once you check out the project, create a `.env.js` in the root of `/twit-service` in the form:

```javscript
   process.env['TWITTER_CONSUMER_KEY'] = 'consumer_key';
   process.env['TWITTER_CONSUMER_SECRET'] = 'consumer_secret';
   process.env['TWITTER_ACCESS_TOKEN'] = 'access_token';
   process.env['TWITTER_ACCESS_SECRET'] = 'access_secret';
   process.env['MONGO_URL'] = 'mongodb://localhost:27017/tweets';
```

## Starting the service
To start the service, simply go to the `/twit-service` directory and type
```
   npm start
```