//if(!process.env.MONGO_URL) {
    //process.env['MONGO_URL'] = 'mongodb://localhost:27017/tweets'
//    process.env['MONGO_URL'] = 'mongodb://mongo:27017/tweets'
//}
if (!process.env.MONGO_SERVICE_HOST) {
    console.log("No mongo service host, running home to localhost");
    process.env.MONGO_SERVICE_HOST = "localhost"
}

if (!process.env.MONGO_SERVICE_PORT) {
    process.env.MONGO_SERVICE_PORT = "27017"
}

process.env['MONGO_URL'] = 'mongodb://' + process.env.MONGO_SERVICE_HOST + ':' + process.env.MONGO_SERVICE_PORT + '/tweets';

console.log("Connecting to mongo at ", process.env.MONGO_URL);

const db = require('monk')(process.env.MONGO_URL);

module.exports = db;